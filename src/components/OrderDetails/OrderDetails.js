import React from 'react';
import ItemDetail from "./ItemDetail/ItemDetail";
import {nanoid} from "nanoid";
import './OrderDetails.css';

const OrderDetails = ({items, removeItem}) => {
   const itemsDetails = [];
   let price = 0;
   const isEmpty = () => {
      if (itemsDetails.length === 0) {
         return (
           <div>
              <p>Order is empty!</p>
              <p>Please add some items!</p>
           </div>
         );
      } else {
         itemsDetails.push(<p key={nanoid()}><strong>Total price: {price} KGS</strong></p>);
         return itemsDetails;
      }

   }
   items.forEach((item) => {
      if (item.amount > 0) {
         price += item.amount * item.price;
         itemsDetails.push(
           <ItemDetail
           amount={item.amount}
           name={item.name}
           key={nanoid()}
           price={item.price}
           currency={item.currency}
           removeItem={() => removeItem(item.id)}
         />)
      }
   });

   return (
     <fieldset className="order-wrapper">
        <legend>Order Details:</legend>
        {isEmpty()}
     </fieldset>
   );
};

export default OrderDetails;