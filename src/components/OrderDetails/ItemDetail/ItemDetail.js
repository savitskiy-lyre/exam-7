import React from 'react';
import './ItemDetail.css';

const ItemDetail = (props) => {
   return (
     <p className="detail-wrapper">
        <span className="detail-name">{props.name}</span>
        <span>{'x' + props.amount}</span>
        <span>{props.price}</span>
        <span>{props.currency.toUpperCase()}</span>
        <button onClick={props.removeItem}>X</button>
     </p>
   );
};

export default ItemDetail;